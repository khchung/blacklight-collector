FROM node:current

ENV blacklight-collector_version=master

RUN apt update && \
    apt upgrade -y && \
    apt install -y \
        bash \
        git \
        ca-certificates \
        fonts-liberation \
        libayatana-appindicator3-1 \
        libasound2 \
        libatk-bridge2.0-0 \
        libatk1.0-0 \
        libc6 \
        libcairo2 \
        libcups2 \
        libdbus-1-3 \
        libexpat1 \
        libfontconfig1 \
        libgbm1 \
        libgcc1 \
        libglib2.0-0 \
        libgtk-3-0 \
        libnspr4 \
        libnss3 \
        libpango-1.0-0 \
        libpangocairo-1.0-0 \
        libstdc++6 \
        libx11-6 \
        libx11-xcb1 \
        libxcb1 \
        libxcomposite1 \
        libxcursor1 \
        libxdamage1 \
        libxext6 \
        libxfixes3 \
        libxi6 \
        libxrandr2 \
        libxrender1 \
        libxss1 \
        libxtst6 \
        lsb-release \
        wget \
        xdg-utils

RUN \
    su node && \
    cd /opt && \
    git clone https://github.com/the-markup/blacklight-collector.git && \
    cd /opt/blacklight-collector && \
    npm ci && \
    npm run build

ENTRYPOINT  ["bash"]
